from django.db import models


class ResPartner(models.Model):
    name = models.CharField(max_length=50, verbose_name="name")

    company_id = models.PositiveIntegerField(verbose_name="company id")

    create_date = models.DateField(verbose_name="create date")

    display_name = models.CharField(max_length=50, verbose_name="display name")

    def __str__(self) -> str:
        return self.display_name

    class Meta:
        verbose_name = "partner resource"
        verbose_name_plural = "partner resources"
