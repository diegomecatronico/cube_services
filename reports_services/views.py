from rest_framework.generics import ListAPIView

from reports_services import models, serializers


class RestPartnerListAPIView(ListAPIView):
    queryset = models.ResPartner.objects.all()
    serializer_class = serializers.RestPartnerSerializer
