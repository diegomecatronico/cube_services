from rest_framework import serializers

from reports_services import models


class RestPartnerSerializer(serializers.ModelSerializer):
    json_input = serializers.JSONField()

    class Meta:
        model = models.ResPartner
        fields = ["name", "id", "display_name"]
