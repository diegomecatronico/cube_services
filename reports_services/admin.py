from django.contrib import admin

from reports_services import models


class ResPartnerAdmin(admin.ModelAdmin):
    list_display = ["name", "display_name"]
    fields = ["name", "display_name", "company_id", "create_date"]


admin.site.register(models.ResPartner, ResPartnerAdmin)
