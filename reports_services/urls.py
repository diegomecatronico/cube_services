from django.urls import path

from reports_services import views

urlpatterns = [
    path("partners/", views.RestPartnerListAPIView.as_view(), name="partner-resources"),
]
