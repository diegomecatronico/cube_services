# Generated by Django 3.2.8 on 2021-11-10 01:49

from django.db import migrations


def create_country_and_stores(apps, schema_editor):
    Country = apps.get_model("bc_api", "Country")
    City = apps.get_model("bc_api", "City")
    Store = apps.get_model("bc_api", "Store")

    colombia = Country.objects.create(name="Colombia", code="CO")
    peru = Country.objects.create(name="Perú", code="PE")
    cr = Country.objects.create(name="Costa Rica", code="CR")
    panama = Country.objects.create(name="Panamá", code="PA")

    cities = [
        {"name": "Bogotá", "country": colombia},
        {"name": "Medellín", "country": colombia},
        {"name": "Barranquilla", "country": colombia},
        {"name": "Lima", "country": peru},
        {"name": "San José", "country": cr},
        {"name": "Ciudad de Panamá", "country": panama},
    ]

    for city in cities:
        City.objects.create(**city)

    stores = [
        {
            "name": "Milla de Oro",
            "code": "mdo",
            "city": City.objects.get(name="Medellín"),
        },
        {"name": "Calle 81", "code": "c81", "city": City.objects.get(name="Bogotá")},
        {"name": "Avenida 19", "code": "a19", "city": City.objects.get(name="Bogotá")},
        {
            "name": "Alto Prado",
            "code": "apo",
            "city": City.objects.get(name="Barranquilla"),
        },
        {"name": "La Mar", "code": "alm", "city": City.objects.get(name="Lima")},
        {"name": "La Encalada", "code": "ale", "city": City.objects.get(name="Lima")},
        {
            "name": "Distrito 4",
            "code": "dge",
            "city": City.objects.get(name="San José"),
        },
        {
            "name": "Calle Uruguay",
            "code": "uru",
            "city": City.objects.get(name="Ciudad de Panamá"),
        },
    ]

    for store in stores:
        Store.objects.create(**store)


class Migration(migrations.Migration):

    dependencies = [
        ("bc_api", "0006_auto_20211022_0140"),
    ]

    operations = [migrations.RunPython(create_country_and_stores)]
