from django.urls import path

from bc_api import views

urlpatterns = [
    path("receptors/", views.ReceptorListView.as_view(), name="receptors"),
    path("receptors/<int:pk>/", views.ReceptorDetail.as_view(), name="receptor-detail"),
    path("stores/", views.StoresListView.as_view(), name="stores"),
    path("stores/<int:pk>/", views.StoreDetailView.as_view(), name="store-detail"),
    path("orders/", views.SaleOrderView.as_view(), name="salesorders"),
    path(
        "orders/<int:pk>/",
        views.SaleOrderDetailUpdateView.as_view(),
        name="saleorder-detail",
    ),
]
