from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from bc_api.models import Receptor, SaleOrder, Store
from bc_api.serializers import ReceptorSerializer, SaleOrderSerializer, StoreSerializer


class JWTAuthMixin:
    permission_classes = (IsAuthenticated,)


class ReceptorListView(JWTAuthMixin, generics.ListCreateAPIView):
    queryset = Receptor.objects.all()
    serializer_class = ReceptorSerializer


class ReceptorDetail(JWTAuthMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = Receptor.objects.all()
    serializer_class = ReceptorSerializer


class StoresListView(JWTAuthMixin, generics.ListAPIView):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer


class StoreDetailView(JWTAuthMixin, generics.RetrieveAPIView):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer


class SaleOrderView(JWTAuthMixin, generics.ListCreateAPIView):
    queryset = SaleOrder.objects.all()
    serializer_class = SaleOrderSerializer


class SaleOrderDetailUpdateView(
    JWTAuthMixin,
    generics.RetrieveAPIView,
    generics.UpdateAPIView,
    generics.GenericAPIView,
):
    serializer_class = SaleOrderSerializer

    def get_queryset(self):
        return SaleOrder.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)
