from django.db import models

from bc_api import data


class Country(models.Model):
    name = models.CharField(verbose_name="Name", max_length=20)

    code = models.CharField(verbose_name="Country Code", max_length=3, unique=True)

    class Meta:
        verbose_name = "country"
        verbose_name_plural = "countries"

    def __str__(self) -> str:
        return self.name


class City(models.Model):
    name = models.CharField(verbose_name="City name", max_length=20)

    country = models.ForeignKey(
        Country, on_delete=models.PROTECT, verbose_name="Country"
    )

    class Meta:
        verbose_name = "city"
        verbose_name_plural = "cities"

    def __str__(self) -> str:
        return f"{self.country.code} - {self.name}"


class Store(models.Model):
    name = models.CharField(verbose_name="Store name", max_length=200)

    code = models.CharField(verbose_name="Code", unique=True, max_length=5)

    city = models.ForeignKey(City, on_delete=models.PROTECT, verbose_name="City")

    is_active = models.BooleanField(default=True, verbose_name="Active")

    class Meta:
        verbose_name = "store"
        verbose_name_plural = "stores"

    def __str__(self) -> str:
        return f"{self.code} - {self.name}"


class Receptor(models.Model):
    name = models.CharField(max_length=200, verbose_name="Salesman name")

    code = models.CharField(unique=True, max_length=50, verbose_name="Code", blank=True)

    employee_id = models.CharField(
        verbose_name="Employee ID", max_length=150, blank=True
    )

    created = models.DateTimeField(auto_now_add=True)

    is_active = models.BooleanField(default=True, verbose_name="Active")

    class Meta:
        verbose_name = "receptor"
        verbose_name_plural = "receptors"
        ordering = ["created"]

    def __str__(self) -> str:
        return self.code


class Customer(models.Model):
    cid = models.CharField(max_length=50, unique=True, verbose_name="ID Customer")

    email = models.CharField(max_length=100, verbose_name="email")

    phone_number = models.CharField(max_length=50, verbose_name="Phone")

    contact = models.CharField(max_length=200, verbose_name="Customer Name")

    class Meta:
        verbose_name = "customer"
        verbose_name_plural = "customers"
        ordering = ["email"]

    def __str__(self) -> str:
        return self.cid


class SaleOrder(models.Model):
    os = models.CharField(max_length=12, unique=True, verbose_name="Order No. ")

    creation_date = models.DateField(verbose_name="Axapta Creation Date")

    delivery_date = models.DateField(verbose_name="Delivery Date")

    customer = models.ForeignKey(
        Customer, verbose_name="Customer", on_delete=models.PROTECT
    )

    store = models.ForeignKey(Store, verbose_name="store", on_delete=models.PROTECT)

    receptor = models.ForeignKey(
        Receptor, verbose_name="Order receptor", on_delete=models.PROTECT
    )

    currency = models.CharField(
        verbose_name="Currency", max_length=3, choices=data.CURRENCY_OPTIONS
    )

    reason_code = models.CharField(
        max_length=20, blank=True, verbose_name="Reason code"
    )

    discount_reason = models.CharField(
        max_length=20, blank=True, verbose_name="Discount reason"
    )

    billing_street = models.CharField(
        max_length=50, blank=True, verbose_name="Billing street"
    )

    shipping_street = models.CharField(
        max_length=50, blank=True, verbose_name="Shipping street"
    )

    state = models.CharField(
        choices=data.SALE_ORDER_STATE_OPTIONS,
        default="OPEN",
        max_length=5,
        verbose_name="Sale order state",
    )

    total_amount = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Total Amount"
    )

    discount = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Discount"
    )

    total_local_currency = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Total local currency"
    )

    exchange_rate = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Exchange rate"
    )

    tax = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="Taxes")

    notes = models.TextField(blank=True)

    sale_origin = models.CharField(
        max_length=20, blank=True, verbose_name="Sale origin"
    )

    os_type = models.CharField(
        max_length=12,
        choices=data.SALE_ORDER_TYPE_OPTIONS,
        default="SALE ORDER",
        verbose_name="Type",
    )

    os_warehouse = models.CharField(
        max_length=12, verbose_name="Warehouse Order No.", blank=True
    )

    # TODO: Evaluate if this field should be in an isolated model.
    warehouse = models.CharField(max_length=12, verbose_name="Warehouse", blank=True)

    deleted_date = models.DateField(verbose_name="Deleted Date", blank=True)

    city = models.CharField(verbose_name="City", max_length=40, blank=True)

    agreement = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="agreement", blank=True
    )

    delivery_address = models.CharField(
        max_length=300,
        verbose_name="Delivery Address",
        blank=True,
        null=True,
    )

    first_follow_up_date = models.DateField(
        blank=True,
        null=True,
    )

    second_follow_up_date = models.DateField(blank=True, null=True)

    third_follow_up_date = models.DateField(
        blank=True,
        null=True,
    )

    fourth_follow_up_date = models.DateField(
        blank=True,
        null=True,
    )

    fifth_follow_up_date = models.DateField(
        blank=True,
        null=True,
    )

    quote_date = models.DateField(
        blank=True,
        null=True,
    )

    quotation_note = models.TextField(blank=True)

    follow_up_date = models.DateField(
        blank=True,
        null=True,
    )

    original_delivery_date = models.DateField(
        blank=True,
        null=True,
    )

    invoiced_amount = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Invoiced amount", default=0
    )

    to_be_invoiced_amount = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="To be invoiced amount", default=0
    )

    invoice_payment = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Invoice payment", default=0
    )

    payed_amount = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Payed amount", default=0
    )

    balance = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Balance", default=0
    )

    invoiced = models.BooleanField(default=False)

    state_delivery = models.CharField(
        max_length=300, verbose_name="Delivery State", blank=True
    )

    state_shipping = models.CharField(
        max_length=300, verbose_name="Delivery State", blank=True
    )

    id_architect = models.CharField(
        max_length=100, verbose_name="ID Architect", blank=True
    )

    architect = models.CharField(
        max_length=100, verbose_name="ID Architect", blank=True
    )

    flag = models.BooleanField(default=False)

    sale_order_invoiced_total = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Amount total", default=0
    )

    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Created At")

    updated_at = models.DateTimeField(auto_now=True, verbose_name="updated at")

    is_active = models.BooleanField(default=True, verbose_name="Active")

    class Meta:
        verbose_name = "Sale order"
        verbose_name_plural = "Sales orders"
        ordering = ["creation_date", "store"]

    def __str__(self) -> str:
        return self.os


class Product(models.Model):
    item_code = models.CharField(
        verbose_name="Product code", max_length=30, unique=True
    )

    product_type = models.SmallIntegerField(
        verbose_name="Product Family", choices=data.PRODUCT_TYPE_CHOICES, default=1
    )

    product_group_code = models.CharField(verbose_name="Group code", max_length=8)

    product_state_code = models.CharField(
        verbose_name="Product state code", max_length=3
    )

    product_description = models.CharField(
        max_length=200, verbose_name="Name", blank=True
    )

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"
        ordering = ["item_code"]

    def __str__(self) -> str:
        return self.item_code


class SaleOrderLine(models.Model):
    axapta_id_code = models.CharField(
        max_length=10, verbose_name="Axapta Line ID", unique=True
    )

    sale_order = models.ForeignKey(
        SaleOrder, verbose_name="OS No: ", on_delete=models.CASCADE
    )

    receptor = models.ForeignKey(
        Receptor, verbose_name="receptor", on_delete=models.PROTECT
    )

    creation_date = models.DateField(verbose_name="Axapta Creation Date")

    delivery_path = models.CharField(
        max_length=30, verbose_name="Delivery path", blank=True
    )

    cm_net_import = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="CM Net Import"
    )

    cm_net_import = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="CM Net Import"
    )

    cm_discount_percentage = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="CM Discount Percentage"
    )

    cm_discount = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="CM Discount"
    )

    cm_discount_reason = models.CharField(
        max_length=30, verbose_name="CM Discount Reason", blank=True
    )

    reason_code = models.CharField(
        max_length=30, verbose_name="Reason code", blank=True
    )

    delivery_date = models.DateField(
        verbose_name="Delivery Date", null=True, blank=True
    )

    taxes_included = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Taxes included"
    )

    quantity = models.SmallIntegerField(verbose_name="Quantity", default=1)

    unit_price = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Unit price"
    )

    taxes = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="Taxes")

    exchange_rate = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Exchange rate"
    )

    is_active = models.BooleanField(default=True, verbose_name="Active")

    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Created At")

    updated_at = models.DateTimeField(auto_now=True, verbose_name="updated at")

    class Meta:
        verbose_name = "Sale order line"
        verbose_name_plural = "Sale order lines"
        ordering = ["axapta_id_code"]

    def __str__(self) -> str:
        return self.axapta_id_code


class StoreTraffic(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE)

    date = models.DateTimeField(verbose_name="Datetime")

    count = models.PositiveIntegerField()

    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Created At")

    updated_at = models.DateTimeField(auto_now=True, verbose_name="updated at")

    class Meta:
        verbose_name = "Store visit measure"
        verbose_name_plural = "Store visit measures"
        ordering = ["date"]
