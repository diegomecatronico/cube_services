from django.contrib import admin

from bc_api import models


class CountryAdmin(admin.ModelAdmin):
    fields = ["name", "code"]


class StoreAdmin(admin.ModelAdmin):
    fields = ["name", "city", "code", "is_active"]
    list_display = ("name", "city", "code", "is_active")


class CityAdmin(admin.ModelAdmin):
    fields = ["name", "country"]
    list_display = ("name", "country")


class ReceptorAdmin(admin.ModelAdmin):
    fields = ["name", "code", "employee_id", "is_active"]
    list_display = ("name", "code", "employee_id", "is_active")


class CustomerAdmin(admin.ModelAdmin):
    fields = ["cid", "email", "phone_number", "contact"]


class SaleOrderAdmin(admin.ModelAdmin):
    fields = ["os", "creation_date", "delivery_date", "customer", "store", "receptor"]
    list_display = ("os", "store", "customer", "receptor", "created_at", "is_active")


admin.site.register(models.Country, CountryAdmin)
admin.site.register(models.City, CityAdmin)
admin.site.register(models.Store, StoreAdmin)
admin.site.register(models.Receptor, ReceptorAdmin)
admin.site.register(models.SaleOrder, SaleOrderAdmin)
admin.site.register(models.Customer, CustomerAdmin)
