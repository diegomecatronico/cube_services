from django.apps import AppConfig


class BcApiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "bc_api"
