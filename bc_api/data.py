CURRENCY_OPTIONS = [
    ("USD", "USD"),
    ("COP", "COP"),
]

SALE_ORDER_STATE_OPTIONS = [
    ("OPEN", "Opened order"),
    ("INV", "Invoiced"),
    ("INVOI", "Delivered"),
]

SALE_ORDER_TYPE_OPTIONS = [
    ("QUOTE", "QUOTE"),
    ("SALE ORDER", "SALE ORDER"),
    ("WORK ORDER", "WORK ORDER"),
]

PRODUCT_TYPE_CHOICES = [(1, "ITEM"), (2, "SERVICE")]
