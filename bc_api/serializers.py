from rest_framework import serializers

from bc_api.models import Customer, Receptor, SaleOrder, Store


class ReceptorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Receptor
        fields = ["code", "name"]


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = ["url", "id", "code", "is_active"]


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ["cid", "email", "phone_number", "contact"]


class CustomerSaleOrderSerializer(serializers.Serializer):
    cid = serializers.CharField(max_length=40)
    email = serializers.CharField(max_length=40)
    phone_number = serializers.CharField(max_length=30)
    contact = serializers.CharField(max_length=30)


class SaleOrderSerializer(serializers.ModelSerializer):
    customer = CustomerSaleOrderSerializer()
    receptor = serializers.SlugRelatedField(
        slug_field="code", queryset=Receptor.objects.all()
    )
    store = serializers.SlugRelatedField(
        slug_field="code", queryset=Store.objects.all()
    )

    class Meta:
        model = SaleOrder
        fields = "__all__"

    def create(self, validated_data):
        customer = validated_data.get("customer")
        receptor_data = validated_data.get("receptor")

        existing_customer = Customer.objects.filter(cid=customer.get("cid")).first()
        existing_receptor = Receptor.objects.filter(
            code=receptor_data.get("receptor")
        ).first()

        if existing_customer:
            new_customer = existing_customer
        else:
            new_customer = Customer.objects.create(**customer)

        if existing_receptor:
            receptor = existing_receptor
        else:
            receptor = Receptor.objects.create(**receptor_data)

        del validated_data["customer"]
        del validated_data["receptor"]
        order = SaleOrder.objects.create(
            **validated_data, customer=new_customer, receptor=receptor
        )
        return order
